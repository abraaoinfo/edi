package br.com.teste.edi.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="solicitacao_transacao")

public class SolicitacaoTransacao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_solicitacao_transacao")
	private Long id;
	
	@JoinColumn(name = "remetente_id_instituicao", unique = true )
	@OneToOne(cascade = CascadeType.ALL)
	private InstituicaoFinanceira remetente;
	
    @OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="destinatario_id_instituicao", unique = true)
	private InstituicaoFinanceira destinatario;
	
	@Column(name="linha_enviada")
	private String linhaEnviada;
	
	@Column(name="linha_devolvida")
	private String linhaDevolvida;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_solicitacao")
	private Date dataSolicitacao;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public InstituicaoFinanceira getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(InstituicaoFinanceira destinatario) {
		this.destinatario = destinatario;
	}

	public String getLinhaEnviada() {
		return linhaEnviada;
	}

	public void setLinhaEnviada(String linhaEnviada) {
		this.linhaEnviada = linhaEnviada;
	}

	public String getLinhaDevolvida() {
		return linhaDevolvida;
	}

	public void setLinhaDevolvida(String linhaDevolvida) {
		this.linhaDevolvida = linhaDevolvida;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public InstituicaoFinanceira getRemetente() {
		return remetente;
	}

	public void setRemetente(InstituicaoFinanceira remetente) {
		this.remetente = remetente;
	}

	

}
