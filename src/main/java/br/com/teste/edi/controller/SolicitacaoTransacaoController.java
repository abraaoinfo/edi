package br.com.teste.edi.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.teste.edi.entity.SolicitacaoTransacao;
import br.com.teste.edi.repository.SolicitacaoTransacaoRepository;
import br.com.teste.edi.rn.ConvertTransacao;


@RestController
@RequestMapping(value = "solicitacao") 
public class SolicitacaoTransacaoController  {
	
	@Autowired
	private SolicitacaoTransacaoRepository solicitacaoTransacaoRepository;
	@Autowired
	private ConvertTransacao convertTransacao;
	
	
	
	@RequestMapping(value = "/converte/transacao", method = RequestMethod.POST, produces=MediaType.TEXT_PLAIN_VALUE)
	public @ResponseBody ResponseEntity<String>  converteSolicitacao (@RequestBody String line) throws IOException{
	
		String linha = null;
		try {
			linha = convertTransacao.convertLinhaInsituicaoDestinataria(StringUtils.trim(line));
		} catch (Exception e) {
			
			return  new ResponseEntity<String>( e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	  
		 return new ResponseEntity<String>(linha, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/converte/transacoes", method = RequestMethod.POST, produces=MediaType.TEXT_PLAIN_VALUE)
	public  @ResponseBody   ResponseEntity<String>  converteSolicitacoes ( HttpServletRequest request,  HttpServletResponse response) throws IOException {

		String linhas =null;
	    try {
			   linhas = convertTransacao.convertLinhasInstituicaoDestinataria(request.getInputStream());
		} catch (Exception e) {
			return  new ResponseEntity<String>( e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	 
		return new ResponseEntity<String>(linhas, HttpStatus.OK); 
		
	}
	
	
	@RequestMapping(value = "/busca/transacoes/remetente/{codigoInstituicao}", method = RequestMethod.GET)
	public   ResponseEntity<List<SolicitacaoTransacao>> buscaSolicitacoesRemetente (@PathVariable("codigoInstituicao") String codigoInstituicao ) {
		
		List<SolicitacaoTransacao>  transacoes = solicitacaoTransacaoRepository.getTransacoesByRemetente(StringUtils.trim(codigoInstituicao));
		
		if(transacoes ==null || transacoes.isEmpty()){
	    	return new ResponseEntity<List<SolicitacaoTransacao>>(HttpStatus.NOT_FOUND);
	    }else{
		
		   return new ResponseEntity<List<SolicitacaoTransacao>>(transacoes, HttpStatus.OK);
	    }
	}

}
