package br.com.teste.edi.rn;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Date;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.teste.edi.beanio.LayoutEntrada;
import br.com.teste.edi.beanio.Linha;
import br.com.teste.edi.beanio.LinhaFactory;
import br.com.teste.edi.entity.InstituicaoFinanceira;
import br.com.teste.edi.entity.SolicitacaoTransacao;
import br.com.teste.edi.repository.InstituicaoFinanceiraRepository;
import br.com.teste.edi.repository.SolicitacaoTransacaoRepository;

@Service
public class ConvertTransacaoImpl implements ConvertTransacao {

	@Autowired
	private InstituicaoFinanceiraRepository ir;
	
	@Autowired
	private SolicitacaoTransacaoRepository solicitacaoTransacaoRepository;
	
	
	private InstituicaoFinanceira  validaInstituicaoRemetente(String line) {

		String codRemetente = line.substring(0, 3);
		InstituicaoFinanceira inst = null;

		if (!StringUtils.isNumeric(codRemetente)) {
			throw new IllegalArgumentException("codigo da institui��o remetente invalido " + codRemetente);
		}

		inst = ir.getInstituicaoFinanceiraById(StringUtils.trim(codRemetente));
		if (inst == null) {
			throw new IllegalArgumentException(
					"codigo da institui��o remetente n�o cadastrado na base " + codRemetente);
		}
	
		return inst;
	}

	private InstituicaoFinanceira validaInstituicaoDestinataria(String line) {
		String trim = line.trim();

		String codDestinataria = line.substring(trim.length()-3);
		InstituicaoFinanceira inst = null;

		if (!StringUtils.isNumeric(codDestinataria.trim())) {
			throw new IllegalArgumentException("codigo da institui��o destinataria invalido " + codDestinataria);
		}

		inst = ir.getInstituicaoFinanceiraById(codDestinataria.trim());
		if (inst == null) {
			throw new IllegalArgumentException(
					"codigo da institui��o destinataria n�o cadastrado na base " + codDestinataria);
		}
		
		
		return inst;
		
	}


	private void gravaSolicitacaoTransacao(String linha, String linhaDevolvida,  
			InstituicaoFinanceira remetente, InstituicaoFinanceira destinataria ){
		
		SolicitacaoTransacao transacao = new SolicitacaoTransacao();
		transacao.setDataSolicitacao(new Date());
		transacao.setLinhaEnviada(linha);
	    transacao.setDestinatario(destinataria);
		transacao.setRemetente(remetente);
		transacao.setLinhaDevolvida(linhaDevolvida);
		
		solicitacaoTransacaoRepository.save(transacao);
	
	}

	@Transactional
	public String convertLinhaInsituicaoDestinataria(String linha) throws Exception  {
		String linhaFormatada = null;
	 try{	
		InstituicaoFinanceira remetente = validaInstituicaoRemetente(linha);
	    InstituicaoFinanceira destinataria = validaInstituicaoDestinataria(linha);
	   
	    LayoutEntrada populaLayout = LayoutEntrada.populaLayout(linha);
	    
	    Linha linhaInstituicao = LinhaFactory.getIntanceLinha(destinataria.getNome());
	    linhaFormatada = linhaInstituicao.linhaFormatada(populaLayout);
	    
	    this.gravaSolicitacaoTransacao(linha, linhaFormatada, remetente, destinataria);
	 }catch(Exception exception){
		 throw new Exception(exception.getMessage(), exception);
	 }
	    return linhaFormatada;
	    
   }
	
	@Transactional
    public String convertLinhasInstituicaoDestinataria(InputStream stream) throws Exception{
    	
    	    BufferedReader reader = new BufferedReader( new InputStreamReader(stream));
    
    	    
	        StringBuilder linhas = new StringBuilder();
	        String linha = "";
	        while( ( linha = reader.readLine() ) != null ){
	            if(StringUtils.isNotBlank(linha)){
	            
	        	String linhaConvertida = convertLinhaInsituicaoDestinataria(linha);
	        	linhas.append(linhaConvertida).append("\n");
	            }
	        }
	        reader.close();
	        return linhas.toString();
    	
    }
	
	
	

}
