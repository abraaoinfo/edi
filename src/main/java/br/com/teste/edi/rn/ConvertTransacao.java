package br.com.teste.edi.rn;

import java.io.InputStream;

public interface ConvertTransacao {
	
	String  convertLinhaInsituicaoDestinataria(String line) throws Exception ;
	String convertLinhasInstituicaoDestinataria(InputStream stream) throws Exception;

}
