package br.com.teste.edi.repository.exception;

public class RepositoryException  extends RuntimeException{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  String repository;


	    public RepositoryException(String repository, String msg) {
	        this(repository, msg, null);
	    }

	    public RepositoryException(String repository, String msg, Throwable cause) {
	        super("[" + (repository == null ? "_na" : repository) + "] " + msg, cause);
	        this.repository = repository;
	    }
	    


	    public RepositoryException(Exception exception) {
           super(exception);
		}

		/**
	     * Returns repository name
	     *
	     * @return repository name
	     */
	    public String repository() {
	        return repository;
	    }
	

}
