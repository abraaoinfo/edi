package br.com.teste.edi.repository.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;

import br.com.teste.edi.repository.exception.RepositoryException;

public abstract class JpaRespositoryImpl<T, I extends Serializable> implements Repository<T, I> {

	@PersistenceContext
	private EntityManager entityManager;

	public T save(T entity) {

		try {
			getEntityManager().persist(entity);
			getEntityManager().flush();
			getEntityManager().refresh(entity);
			return entity;

		} catch (ConstraintViolationException e) {
			throw new RepositoryException(e);
		} catch (Exception e) {
			throw new RepositoryException(e);
		}
	}

	public T update(T entity) {
		T updatedEntity;
		try {
			updatedEntity = getEntityManager().merge(entity);
		} catch (ConstraintViolationException e) {
			throw new RepositoryException(e);
		} catch (Exception e) {
			throw new RepositoryException(e);
		}
		return updatedEntity;

	}

	public void remove(Class<T> classe, T id) {
		try {
			T ref = getEntityManager().find(classe, id);
			getEntityManager().remove(ref);
		} catch (Exception e) {
			throw new RepositoryException(e);
		}
	}

	public T getById(Class<T> classe, I pk) {

		try {
			return getEntityManager().find(classe, pk);
		} catch (NoResultException e) {
			throw new RepositoryException(e);
		} catch (Exception e) {
			throw new RepositoryException(e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll(Class<T> classe) {
		List<T> resultList = null;

		try {
			resultList = getEntityManager().createQuery("select o from " + classe.getSimpleName() + " o")
					.getResultList();
		} catch (Exception e) {
			throw new RepositoryException(e);
		}
		return resultList;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
