package br.com.teste.edi.repository.impl;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.teste.edi.entity.SolicitacaoTransacao;
import br.com.teste.edi.repository.SolicitacaoTransacaoRepository;
import br.com.teste.edi.repository.jpa.JpaRespositoryImpl;

@Repository
public class SolicitacaoTransacaoRepositoryImpl extends JpaRespositoryImpl<SolicitacaoTransacao, Long> 
 implements SolicitacaoTransacaoRepository {

	@SuppressWarnings("unchecked")
	public List<SolicitacaoTransacao>  getTransacoesByRemetente(String codInstituicao) {
		 List<SolicitacaoTransacao> resultList =null;
		 List<String>  linhas = null;
		 
		try{
			resultList = this.getEntityManager().
		        		
				createQuery("SELECT  s FROM SolicitacaoTransacao s  WHERE s.remetente.codInstituicao like :codInstituicao")
	           .setParameter("codInstituicao","%" + codInstituicao+ "%").getResultList();
		}catch(Exception ex){
			return null;
		}	


	
		return resultList;
		
	}
	
	

}
