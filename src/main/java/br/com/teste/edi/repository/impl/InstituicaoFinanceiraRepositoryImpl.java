package br.com.teste.edi.repository.impl;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import br.com.teste.edi.entity.InstituicaoFinanceira;
import br.com.teste.edi.repository.InstituicaoFinanceiraRepository;
import br.com.teste.edi.repository.jpa.JpaRespositoryImpl;

@Repository
public class InstituicaoFinanceiraRepositoryImpl extends JpaRespositoryImpl<InstituicaoFinanceira, Long> 
  implements InstituicaoFinanceiraRepository{
	
	public  InstituicaoFinanceira getInstituicaoFinanceiraById(String codInstituicao) {
		
		InstituicaoFinanceira financeira=null;
		try{
			
			financeira =(InstituicaoFinanceira) this.getEntityManager().createQuery("SELECT i FROM InstituicaoFinanceira i "
					+ "WHERE i.codInstituicao= :codInstituicao")
		    .setParameter("codInstituicao", codInstituicao).getSingleResult();
		}catch(NoResultException ex){
			 return null;
		}
		return financeira;
	}
	
	

	

}
