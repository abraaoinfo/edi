package br.com.teste.edi.repository;

import br.com.teste.edi.entity.InstituicaoFinanceira;
import br.com.teste.edi.repository.jpa.Repository;

public interface InstituicaoFinanceiraRepository extends Repository<InstituicaoFinanceira, Long> {
	
	 InstituicaoFinanceira getInstituicaoFinanceiraById(String codInsituicao);

}
