package br.com.teste.edi.repository;

import java.util.List;

import br.com.teste.edi.entity.SolicitacaoTransacao;
import br.com.teste.edi.repository.jpa.Repository;

public interface SolicitacaoTransacaoRepository extends Repository<SolicitacaoTransacao, Long>{
	
	public List<SolicitacaoTransacao>  getTransacoesByRemetente(String codInstituicao);

}
