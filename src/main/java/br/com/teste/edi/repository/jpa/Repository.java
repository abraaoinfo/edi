package br.com.teste.edi.repository.jpa;

import java.io.Serializable;
import java.util.List;

public interface Repository<T, I extends Serializable> {
	
   public T save (T Entity);
   public void remove(Class<T> classe,T id);
   public T getById(Class<T> classe, I pk);
   public List<T> getAll(Class<T> classe);
	

}
