package br.com.teste.edi.beanio;

public class LinhaItau implements Linha {
	
	/**
	 * Linha padrao itau
	 * 
	 * */
	

	public String linhaFormatada(LayoutEntrada entrada) {
		
		StringBuilder builder = new StringBuilder(entrada.getCpf()).
				append(entrada.getNome()).append(entrada.getAgencia()).append(entrada.getDigitoConta()
		).append(entrada.getValorDecimais()).append(entrada.getTipoTransacao()).append(entrada.getData())
				.append(entrada.getInstituicaoDestinataria());
	
		return builder.toString();
	}

}
