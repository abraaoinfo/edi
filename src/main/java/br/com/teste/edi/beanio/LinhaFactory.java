package br.com.teste.edi.beanio;

import org.apache.commons.lang3.StringUtils;

public class LinhaFactory {
	

		public static Linha getIntanceLinha(String nameInstituicao){
		
			String nome = StringUtils.capitalize(StringUtils.trim(nameInstituicao));
			Linha linha =null;
			try {
				linha = (Linha) Class.forName ("br.com.teste.edi.beanio.Linha"+nome).newInstance();
			} catch (InstantiationException e) {
			
			} catch (IllegalAccessException e) {
			
			} catch (ClassNotFoundException e) {
				
			}
		
		return linha;
	    
	    
	}

}
