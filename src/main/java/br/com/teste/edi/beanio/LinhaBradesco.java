package br.com.teste.edi.beanio;

public class LinhaBradesco implements Linha {
	
	
	/** 
	 * Linha padrao bradesco
	 * 
	 * */

	public String linhaFormatada(LayoutEntrada entrada) {
		
		StringBuilder builder = new StringBuilder(entrada.getNome()).
				append(entrada.getCpf()).append(entrada.getAgencia()).append(entrada.getDigitoConta()
		).append(entrada.getValorDecimais()).append(entrada.getTipoTransacao()).append(entrada.getData())
				.append(entrada.getInstituicaoDestinataria());
		
		return builder.toString();
	}

}
