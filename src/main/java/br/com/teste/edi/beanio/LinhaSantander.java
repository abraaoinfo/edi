package br.com.teste.edi.beanio;

public class LinhaSantander implements Linha {

	
	
	/**
	 * Padr�o de linha santander
	 */

	public String linhaFormatada(LayoutEntrada entrada) {
		
		StringBuilder builder = new StringBuilder(entrada.getTipoTransacao()).
	append(entrada.getCpf()).append(entrada.getAgencia()).append(entrada.getDigitoConta()
		+entrada.getNome()).append(entrada.getData()).append(entrada.getInstituicaoDestinataria());
		
		return builder.toString();
		
	}

}
