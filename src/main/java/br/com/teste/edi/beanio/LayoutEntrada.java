package br.com.teste.edi.beanio;

import org.beanio.BeanIOConfigurationException;
import org.beanio.InvalidRecordException;
import org.beanio.StreamFactory;
import org.beanio.Unmarshaller;
import org.beanio.annotation.Field;
import org.beanio.annotation.Record;
import org.beanio.builder.StreamBuilder;
import org.springframework.stereotype.Service;

@Service
@Record(minOccurs = 1)


public class LayoutEntrada {
	
	private final String NUMERICO = "^[0-9\\s]+$";
	private final String LETRAS = "^[a-zA-Z\\s]+$";
	
	@Field(at = 0, length = 3, regex=NUMERICO)
	private String  instituicaoRemente;

	@Field(at = 3, length=3, trim=true, regex=LETRAS)
	private String  tipoTransacao;
	
	@Field(at = 6, length = 4, regex=NUMERICO)
	private String agencia;
	
	@Field(at = 10, length = 5, regex=NUMERICO)
	private String conta ;
	
	@Field(at = 11, length = 1, regex=NUMERICO)
	private String digitoConta ;
	
    @Field(at = 16,  length=30, minLength=15, regex=LETRAS)
	private String nome;
	
	@Field(at = 46, length = 11,  regex=NUMERICO)
	private String cpf ;
	
	@Field(at = 57, length = 5,  regex=NUMERICO)
	private String valor;
	
	@Field(at = 62, length = 2,  regex=NUMERICO)
	private String valorDecimais;
	
	@Field(at =64 , length = 8, regex=NUMERICO)
	private String data;
	
	@Field(at =72, length = 3, regex=NUMERICO)
	private String instituicaoDestinataria;
	
	public String getTipoTransacao() {
		return tipoTransacao;
	}
	public void setTipoTransacao(String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getDigitoConta() {
		return digitoConta;
	}
	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getValorDecimais() {
		return valorDecimais;
	}
	public void setValorDecimais(String valorDecimais) {
		this.valorDecimais = valorDecimais;
	}

	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

	public String getInstituicaoDestinataria() {
		return instituicaoDestinataria;
	}
	public void setInstituicaoDestinataria(String instituicaoDestinataria) {
		this.instituicaoDestinataria = instituicaoDestinataria;
	}

	
	public static LayoutEntrada populaLayout(String linha) { 
		LayoutEntrada layoutEntrada = null;
		
	 try{	
		    StreamBuilder builder = new StreamBuilder("s1").format("fixedlength").addRecord(LayoutEntrada.class);
		    StreamFactory factory = StreamFactory.newInstance();
		    factory.define(builder);
		    Unmarshaller unmarshaller = factory.createUnmarshaller("s1");
		    layoutEntrada = (LayoutEntrada) unmarshaller.unmarshal(linha.trim());
 	 }catch(BeanIOConfigurationException ex){
		throw new BeanIOConfigurationException("Layout invalido");
	 }catch(InvalidRecordException ex){
		 throw new InvalidRecordException(ex.getRecordContext(), "Erro no layout");
	 }
	
	 return layoutEntrada;
	}
	
	public static void main(String[] args) {

		
		String line = "314DOC8847182098Abraao francisco da silva     31450374808451237218112017033";
		populaLayout(line);
	}
	
}
