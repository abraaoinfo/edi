package br.com.teste.edi.beanio;

public interface Linha {
	
	public String linhaFormatada(LayoutEntrada entrada);

}
